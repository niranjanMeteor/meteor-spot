Meteor.methods({
	registerEmployer : function(data) {
		if ( ! Meteor.call('verify_name', data.name) ) {
			return false;
		}
		if ( ! Meteor.call('verify_email', data.email) ) {
			return false;
		}
		if ( ! Meteor.call('verify_password', data.password) ) {
			return false;
		}

		var newEmloyerId = Accounts.createUser({
			email : data.email,
			password : data.password,
			profile : {
				name : data.name,
				is_admin : data.is_admin
			},
			user_type : "employer"
		});
		if (newEmloyerId) {
			return true;
		}
		return false;
	},

	registerEmployee : function(data) {
		if ( ! Meteor.call('verify_name', data.name) ) {
			return false;
		}
		if ( ! Meteor.call('verify_email', data.email) ) {
			return false;
		}
		if ( ! Meteor.call('verify_password', data.password) ) {
			return false;
		}
		if ( ! Meteor.call('verify_job_role', data.job_role) ) {
			return false;
		}
		if ( ! Meteor.call('verify_team_name', data.team) ) {
			return false;
		}
		if ( ! Meteor.call('verify_project_name', data.project) ) {
			return false;
		}

		new_seat_number = Meteor.call('getSeatForNewEmployee');
	
		var newEmployeeId = Accounts.createUser({
			email : data.email,
			password : data.password,
			profile : {
				name : data.name,
				team : data.team,
				project : data.project,
				job_role : data.job_role
			},
			user_type : "employee",
			seat_number : new_seat_number
		});
		if (newEmployeeId) {
			if ( ! Teams.findOne({name : data.team})) {
				Teams.insert({'name' : data.team});
			}
			if ( ! JobRoles.findOne({role_name : data.job_role})) {
				JobRoles.insert({'role_name' : data.job_role});
			}
			return true;
		}
		return false;
	},

	verify_name : function (name) {
		var name_regex = /^[A-Za-z0-9 ]+$/;
		return ( ! Meteor.call('empty', name) && name_regex.test(name) );
	},
	verify_email : function (email) {
		var email_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		return ( ! Meteor.call('empty', email) && email_regex.test(email) );
	},
	verify_password : function (password) {
		return ( ! Meteor.call('empty', password) && password.length >= 2 );
	},
	verify_job_role : function (job_role) {
		var job_role_regex = /^[A-Za-z0-9 ]+$/;
		return ( ! Meteor.call('empty', job_role) && job_role_regex.test(job_role) );
	},
	verify_team_name : function (team_name) {
		var team_name_regex = /^[A-Za-z0-9 ]+$/;
		return ( ! Meteor.call('empty', team_name) && team_name_regex.test(team_name) );
	},
	verify_project_name : function (project_name) {
		var project_name_regex = /^[A-Za-z0-9 ]+$/;
		return ( ! Meteor.call('empty', project_name) && project_name_regex.test(project_name) );
	},

	getSeatForNewEmployee : function () {
		var occupied_seats = [];
		var count = 0;
		Meteor.users.find({},{fields :{'seat_number':1,'_id':1}}).forEach(function (seat) {
			occupied_seats[count] = seat.seat_number;
			count++;
		});
		if ( count > 0) {
			for (var i=1 ;i <=16 ;i++) {
				if (occupied_seats.indexOf(i+'') < 0) {
					return i+'';
				}
			}
		} else {
			return '1';
		}
	}
});