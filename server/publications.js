Meteor.publish("users", function () {
  if (this.userId) {
    records = Meteor.users.find({},{fields :{'createdAt':0,'services':0}});
    return records;
  } else {
    this.ready();
  }
  
});

Meteor.publish("teams", function () {
	return Teams.find();
});

Meteor.publish("job_roles", function () {
	return JobRoles.find();
});