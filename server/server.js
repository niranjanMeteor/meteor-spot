Meteor.methods({

  empty : function (data) {
    if(typeof(data) == 'number' || typeof(data) == 'boolean') {
      return false;
    }
    if(typeof(data) == 'undefined' || data === null) {
      return true;
    }
    if(typeof(data.length) != 'undefined') {
      return data.length == 0;
    }
    var count = 0;
    for(var i in data) {
      if(data.hasOwnProperty(i)) {
        count ++;
      }
    }
    return count == 0;
  },

  updateUserSeat : function (old_seat, new_seat) {
    Meteor.users.find({"seat_number" : old_seat},{fields :{'_id':1}}).forEach(function (seat_user) {
      user_id = seat_user._id;
    });
    Meteor.users.update({"_id" : user_id},{ $set : {"seat_number" : new_seat}});
  }
  
});

