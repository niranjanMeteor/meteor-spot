//add balance, initial 0
Accounts.onCreateUser(function(options, user) {
  user.user_type = options.user_type;
  user.seat_number = options.seat_number;
  if (options.profile)
    user.profile = options.profile;
  return user;
});