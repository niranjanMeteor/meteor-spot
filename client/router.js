Router.configure({
	layoutTemplate : 'login_layout'
});

Router.route('/' , function () {
	if (Meteor.userId()) {
		Router.go('home');
	} else {
		this.layout('login_layout');
	}
});

Router.route('/login' , function () {
	if (Meteor.userId()) {
		Router.go('home');
	} else {
		this.layout('login_layout');
	}
});

Router.route('/register' , function () {
	if (Meteor.userId()) {
		Router.go('home');
	} else {
		this.layout('register_layout');
	}
});

Router.route('/logout' , function () {
	Meteor.logout();
	Router.go('login');
});

Router.route('/new_register' , function () {
	Meteor.logout();
	this.layout('register_layout');
});

Router.route('/home' , function () {
	if (Meteor.userId()) {
		if (Meteor.user().user_type === "employer") {
			this.layout('home_layout_employer');
		} else if (Meteor.user().user_type === "employee") {
			this.layout('home_layout_employee');
		}
	} else {
		Router.go('login');
	}
});

Router.route('/home2' , function () {
	this.layout('home_layout_employee');
});
