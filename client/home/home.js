Template.home_header.rendered = function () {
  $(document).ready(function(){
    $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: false, 
      gutter: 0, 
      belowOrigin: false 
    });
  });
}

Template.home_body_employer.rendered = function () {
  $(document).ready(function(){
    $('.modal-trigger').leanModal();
  });
  Meteor.users.find({"user_type": "employee"},{fields :{'seat_number':1,'_id':1}}).forEach(function (seat) {
    $('#'+seat.seat_number).addClass('seat-occupied-color');
    if (Meteor.user().profile.is_admin == true) {
      $('#'+seat.seat_number).attr('draggable', true);
    }
  });
}

Template.home_header.helpers({
  user_name : function () {
    return Meteor.user().profile.name;
  }
});

Template.home_body_employer.events({
  'dragstart .seat-size' : function (event) {
    event.originalEvent.dataTransfer.setData("Text", event.target.id);
  },
  'dragover .seat-size' : function (event) {
    event.preventDefault();
  },
  'drop .seat-size' : function (event) {
    event.preventDefault();
    var old_seat = event.originalEvent.dataTransfer.getData("Text");
    var new_seat = event.target.id;
    if ( ! Meteor.users.findOne({"seat_number" : new_seat}) ) {
      if (new_seat !== old_seat) {
        Meteor.call('updateUserSeat',old_seat,new_seat, function(err, result) {
          if (err) {
            Materialize.toast('User Seat Could not be Updated :', 2000);
          } else {
            Materialize.toast('User Seat updated Succesfully :', 2000);
            Router.go('home');
          }
        });
      } else {
        Materialize.toast('Old and New Seat Are Same:', 2000);
      }
    } else {
      Materialize.toast('New Seat is Already Occupied', 2000);
    }
  },
	'click .dropdown-button' : function () {
    event.preventDefault();
    var clicked_seat = event.target.id;
    if (clicked_seat === 'search') {
      selector = {"profile.name" : $('#search').val().trim(), "user_type" : "employee"};
    } else {
      selector = {"seat_number" : clicked_seat, "user_type" : "employee"};
    }
    selectedUser = Meteor.users.find(selector,{fields :{'profile':1,'_id':1}});
    if (selectedUser.count() > 0) {
      selectedUser.forEach(function (clickedUser) {
        Session.set('employee_name',clickedUser.profile.name);
        Session.set('employee_team',clickedUser.profile.team);
        Session.set('employee_project',clickedUser.profile.project);
      });
    } else {
      $('#dropdown1').hide();
      Session.set('employee_name','');
      Session.set('employee_team','');
      Session.set('employee_project','');
    }
  },
  'keypress #search' : function (event) {
    if (event.which == 13) {
      event.preventDefault();
      $('#dropdown1').hide();
      var search_text = $('#search').val();

      for (var j=1 ; j<=16 ; j++) {
        $('#'+j+'').removeClass('seat-filter-color');
      }

      searched_employee = Meteor.users.find({"profile.name" : search_text},{fields :{'seat_number':1,'_id':1}});
      if (searched_employee.count() > 0) {
        searched_employee.forEach(function (seat) {
          $('#'+seat.seat_number+'').addClass('seat-filter-color');
          $('#'+seat.seat_number+'').click();
        });
      } else {
        Materialize.toast('No Employee Exists by this name', 2000);
      }
    } 
  },
  'submit .filter-form' : function (event) {
    event.preventDefault();

    var team = event.target.team_group.value;
    jobRole = event.target.job_group.value;
    var selector;
    if (team === "all_teams") {
      if (jobRole === "all_jobs") {
        selector = {};
      } else {
        selector = {
          "profile.job_role" : jobRole
        };
      }
    } else {
      if (jobRole === "all_jobs") {
        selector = {
          "profile.team" : team
        };
      } else {
        selector = {
          "profile.team" : team , "profile.job_role" : jobRole
        };
      }
    }
    for (var j=1 ; j<=16 ; j++) {
      $('#'+j+'').removeClass('seat-filter-color');
    }
    Meteor.users.find(selector,{fields :{'seat_number':1,'_id':1}}).forEach(function (seat) {
      $('#'+seat.seat_number+'').addClass('seat-filter-color');
    });
  }
});

Template.home_body_employer.helpers({
  isUserAdmin : function () {
    if (Meteor.user().profile.is_admin == true) {
      return true;
    } else {
      return false;
    }
  },
  teams : function () {
    return Teams.find();
  },
  job_roles : function () {
    return JobRoles.find();
  },
  employeeExists : function () {
    if ( ! empty(Session.get('employee_name'))) {
      return true;
    }
    return false;
  },
  employeeName : function () {
    return Session.get('employee_name');
  },
  employeeTeam : function () {
    return Session.get('employee_team');
  },
  employeeProject : function () {
    return Session.get('employee_project');
  }
});

