Template.register_form.rendered = function () {
  $(document).ready(function(){
    $('.employer_reg_form').hide();
    $('.employee_reg_form').show();
    $('.employee-reg').addClass('register-as-employer');
  });
}

Template.register_form.events({

	'click .employer-reg' : function (event) {
		$('.employer_reg_form').show();
    $('.employee_reg_form').hide();
    $('.employer-reg').addClass('register-as-employer');
    $('.employee-reg').removeClass('register-as-employee');
	},

	'click .employee-reg' : function (event) {
		$('.employer_reg_form').hide();
    $('.employee_reg_form').show();
    $('.employee-reg').addClass('register-as-employee');
    $('.employer-reg').removeClass('register-as-employer');
	},

	'submit .employer_reg_form' : function (event) {
		event.preventDefault();

		var name = event.target.name.value.trim();
		if ( ! verify_name(name) ) {
			Materialize.toast('Name Should contain letters and numbers only!', 4000);
			return false;
		}

		var email = event.target.email.value.trim();
		if ( ! verify_email(email) ) {
			Materialize.toast('Email format is not valid', 4000);
			return false;
		}

		var password = event.target.password.value.trim();
		if ( ! verify_password(password) ) {
			Materialize.toast('Password should be of atleast 3 characters', 4000);
			return false;
		}

		if ( Meteor.users.findOne({"profile.name" : name}) ) {
			Materialize.toast('Name Already Exists ,Input a different name', 4000);
			return false;
		}

		if ( Meteor.users.findOne({"email" : email}) ) {
			Materialize.toast('Email Already Exists ,Input a different email', 4000);
			return false;
		}

		var is_admin = event.target.is_admin.checked;

		var register_data = {
			email : email,
			password : password,
			name : name,
			is_admin : is_admin
		};

		Meteor.call('registerEmployer', register_data, function(err, result) {
			if (err) {
				Materialize.toast(err, 4000);
			} else {
				Meteor.loginWithPassword(email,	password, function(err) {
					if (err) {
						Materialize.toast('Invalid Login Credentials', 4000);
					} else {
						Router.go('home');
					}
				});
			}
		});
		return false;
	},

	'submit .employee_reg_form' : function (event) {
		event.preventDefault();

		var name = event.target.name.value.trim();
		if ( ! verify_name(name) ) {
			Materialize.toast('Name Should contain letters and numbers only!', 4000);
			return false;
		}

		var email = event.target.email.value.trim();
		if ( ! verify_email(email) ) {
			Materialize.toast('Email format is not valid', 4000);
			return false;
		}

		var password = event.target.password.value.trim();
		if ( ! verify_password(password) ) {
			Materialize.toast('Password should be of atleast 3 characters', 4000);
			return false;
		}

		var job_role = event.target.job_role.value.trim();
		if ( ! verify_job_role(job_role) ) {
			Materialize.toast('Job Role Should contain letters and numbers only!', 4000);
			return false;
		}

		var team_name = event.target.team_name.value.trim();
		if ( ! verify_team_name(team_name) ) {
			Materialize.toast('Team Name Should contain letters and numbers only!', 4000);
			return false;
		}

		var project_name = event.target.project_name.value.trim();
		if ( ! verify_project_name(project_name) ) {
			Materialize.toast('Project Name Should contain letters and numbers only!', 4000);
			return false;
		}

		if ( Meteor.users.findOne({"profile.name" : name}) ) {
			Materialize.toast('Name Already Exists ,Input a different name', 4000);
			return false;
		}

		if ( Meteor.users.findOne({"email" : email}) ) {
			Materialize.toast('Email Already Exists ,Input a different email', 4000);
			return false;
		}

		if ( Meteor.users.find({"user_type" : "employee"}).count() >= 15 ) {
			Materialize.toast('No More Employee Can Be Created, All Seats Full', 4000);
			return false;
		}

		var register_data = {
			email : email,
			password : password,
			name : name,
			job_role : job_role,
			team : team_name,
			project : project_name
		};

		Meteor.call('registerEmployee', register_data, function(err, result) {
			if (err) {
				Materialize.toast(err, 4000);
			} else {
				Materialize.toast('New Employee Successfully Registered', 4000);
				Meteor.loginWithPassword(email,	password, function(err) {
					if (err) {
						Materialize.toast('Invalid Login Credentials', 4000);
					} else {
						Router.go('home');
					}
				});
			}
		});
		return false;
	}
});


Meteor.methods({
	registerEmployer : function(user) {
		return true;
	},
	registerEmployee : function(user) {
		return true;
	}
});

