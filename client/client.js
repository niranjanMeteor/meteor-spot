
empty = function (data) {
  if(typeof(data) == 'number' || typeof(data) == 'boolean') {
    return false;
  }
  if(typeof(data) == 'undefined' || data === null) {
    return true;
  }
  if(typeof(data.length) != 'undefined') {
    return data.length == 0;
  }
  var count = 0;
  for(var i in data) {
    if(data.hasOwnProperty(i)) {
      count ++;
    }
  }
  return count == 0;
};

verify_name = function (name) {
  var name_regex = /^[A-Za-z0-9 ]+$/;
  if (!empty(name) && name_regex.test(name)) {
    return true;
  } else {
    Session.set('name_validation_message', 'Invalid name');
    return false;
  }
};

verify_email = function (email) {
  var email_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!empty(email) && email_regex.test(email)) {
    return true;
  } else {
    Session.set('email_validation_message', 'Invalid Email Id');
    return false;
  }
};

verify_password = function (password) {
  if (!empty(password) && password.length >= 2) {
    return true;
  } else {
    Session.set('password_validation_message');
    return false;
  }
};

verify_job_role = function (job_role) {
  var job_role_regex = /^[A-Za-z0-9 ]+$/;
  if (!empty(job_role) && job_role_regex.test(job_role)) {
    return true;
  } else {
    Session.set('job_role_validation_message', 'Invalid job_role');
    return false;
  }
};

verify_team_name = function (name) {
  var name_regex = /^[A-Za-z0-9 ]+$/;
  if (!empty(name) && name_regex.test(name)) {
    return true;
  } else {
    Session.set('name_validation_message', 'Invalid name');
    return false;
  }
};

verify_project_name = function (name) {
  var name_regex = /^[A-Za-z0-9 ]+$/;
  if (!empty(name) && name_regex.test(name)) {
    return true;
  } else {
    Session.set('name_validation_message', 'Invalid name');
    return false;
  }
};

