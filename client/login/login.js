Template.login_form.events({

	'submit form' : function (event) {
		event.preventDefault();

		var email = event.target.email.value.trim();
		if ( ! verify_email(email) ) {
			return false;
		}

		var password = event.target.password.value.trim();
		if ( ! verify_password(password) ) {
			return false;
		}

		Meteor.loginWithPassword(email,	password, function(err) {
			if (err) {
				Materialize.toast('Invalid Login Credentials', 4000);
			} else {
				Router.go('home');
			}
		});

	}

});

